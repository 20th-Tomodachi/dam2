
public class t1ej2 extends Thread
{

   private String nombre;
   
   public t1ej2(String nombre)
   {
     this.nombre = nombre;      
   }

   public void run()
   {
     try
     {
       int x = (int) (Math.random()*5000)+1;
       Thread.sleep(x);
       System.out.println("Soy: "  + nombre + " (" + x + ")" );
     }
     catch ( Exception ex)
     {
       ex.printStackTrace();
     }
   }

   public static void main ( String[] args )
   {
     t1ej2 t1 = new t1ej2("Pedro");
     t1ej2 t2 = new t1ej2("Pablo");
     t1ej2 t3 = new t1ej2("Juan");

     t1.start();
     t2.start();
     t3.start();
   }

}