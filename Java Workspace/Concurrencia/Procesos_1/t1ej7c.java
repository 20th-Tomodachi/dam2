
public class t1ej7c implements Runnable
{

    private static char[] nuestroMar = { '*','*','*','*','*','*' ,'*','*','*','*' };
    int posicion;

    public t1ej7c(int pos)
    {
      posicion = pos; 
      nuestroMar[posicion] = 'B';
    }

    synchronized void desplazar()
    {

       int direccion = (int)(Math.random()*2);  // 0,1 : derecha, izquierda

       if ( direccion == 0 && posicion<9 && nuestroMar[posicion+1] == '*' ) 
       {
          nuestroMar[posicion] = '*';
          posicion++;
          nuestroMar[posicion] = 'B';
       }
       else if ( direccion ==1 && posicion>0 && nuestroMar[posicion-1] == '*' ) 
       {
          nuestroMar[posicion] = '*';
          posicion--;
          nuestroMar[posicion] = 'B';
       }

    }

   synchronized static void visualizar()
   {
        for ( int i=0; i<10; i++) System.out.print(nuestroMar[i]);
        System.out.println(); 
   }

   public void run()
   {
      while (true) 
      {
        this.desplazar();
        visualizar();
      }
   }


   public static void main( String[] args )
   {

     Thread b1 = new Thread (new t1ej7c(3));
     Thread b2 = new Thread (new t1ej7c(8));

     b1.setPriority(10);
     b2.setPriority(1);

     b1.start();
     b2.start();

   }

}

 

    

 


