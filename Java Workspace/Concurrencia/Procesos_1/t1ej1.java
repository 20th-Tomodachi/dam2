
public class t1ej1 
{

   private String nombre;
   
   public t1ej1(String nombre)
   {
     this.nombre = nombre;      
   }

   public void run()
   {
     try
     {
       int x = (int) (Math.random()*5000)+1;
       Thread.sleep(x);
       System.out.println("Soy: "  + nombre + " (" + x + ")" );
     }
     catch ( Exception ex)
     {
       ex.printStackTrace();
     }
   }

   public static void main ( String[] args )
   {
     t1ej1 t1 = new t1ej1("Pedro");
     t1ej1 t2 = new t1ej1("Pablo");
     t1ej1 t3 = new t1ej1("Juan");

     t1.run();
     t2.run();
     t3.run();
   }

}