
public class t1ej5 implements Runnable
{

   private String nombre;
   
   public t1ej5(String nombre)
   {
     this.nombre = nombre;      
   }

   public void run()
   {
     try
     {
       int x = (int) (Math.random()*5000)+1;
       Thread.sleep(x);
       System.out.println("Soy: "  + nombre + " (" + x + ")" );
     }
     catch ( Exception ex)
     {
       ex.printStackTrace();
     }
   }

   public static void main ( String[] args )
   {
     Thread t1 = new Thread( new t1ej5("Pedro") );
     Thread t2 = new Thread( new t1ej5("Pablo") );
     Thread t3 = new Thread( new t1ej5("Juan" ) );

     t1.start();
     t2.start();
     t3.start();

     try           // el compilador me obliga al try..
     {
      t1.join();
      t2.join();
      t3.join();
     }
     catch ( Exception ex) {}

     System.out.println("El programa SEGURO que ha terminado"); 

   }

}