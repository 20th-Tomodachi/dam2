package productorConsumidor4;

public class Consumidor implements Runnable {
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	
	Cola cola;
	
	// ...
    
    public Consumidor(Cola cola) {this.cola = cola;}

	public void run() {
		while (!finalizado) {
			Integer dato = cola.retirar();
			if (dato != null)	{/*Procesar el dato*/}
		}
	}
    
}
