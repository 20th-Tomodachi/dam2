package productorConsumidor4;

public class Productor implements Runnable {
	
	final Integer ValorMinimo = 1; 
	final Integer ValorMaximo = 10; 
	
	Integer dato;
	boolean datoEncolado = true;
	Cola cola;
	
	// ...
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	

	
	// ...
    
    public Productor(Cola cola) {
    	this.cola = cola;
    }

	public void run() {

		while (!finalizado) {
			if (datoEncolado) {
				int amplitudRango = ValorMaximo - ValorMinimo + 1;
				int aleatorio = (int) (Math.random() * amplitudRango); // 0..9
				dato = ValorMinimo + aleatorio;
				datoEncolado=false;
			}
			datoEncolado = cola.almacenar(dato);
		}

	}
    
}
