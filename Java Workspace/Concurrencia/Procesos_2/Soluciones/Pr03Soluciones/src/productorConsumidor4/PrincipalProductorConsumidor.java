package productorConsumidor4;

public class PrincipalProductorConsumidor {

	public static void main(String[] args) {
		
		Cola cArray = new Cola(5);
		
		new Thread(new Productor(cArray)).start();
		new Thread(new Consumidor(cArray)).start();
	}
}
