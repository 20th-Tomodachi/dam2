package productorConsumidor4;

import java.util.Arrays;

public class Cola {

	private Integer[] cola;
	
	private int tamanyoCola;
	private int numElementosEnCola;
	private int indiceProductor=0;
	private Integer indiceConsumidor = null;
	
	public Cola(int dimension){
		this.cola = new Integer[dimension];
		this.tamanyoCola = dimension;
		this.numElementosEnCola = 0;
		this.indiceProductor=0;
		this.indiceConsumidor = 0;
	}
	
	public synchronized boolean almacenar(Integer dato){
		if(numElementosEnCola<tamanyoCola){
                    cola[indiceProductor] = dato; /*print para pruebas*/ System.out.println("Insertando ("+dato+")");  System.out.println(Arrays.toString(cola));
                    numElementosEnCola++;
                    indiceProductor=(indiceProductor+1)%tamanyoCola;
                    return true;
		}
		return false;
		
	}

	public synchronized Integer retirar(){
		if(numElementosEnCola>0){
					Integer dato = cola[indiceConsumidor];  /**/ System.out.println("Consumido " + dato);  System.out.println(Arrays.toString(cola));
                    numElementosEnCola--;
                    indiceConsumidor = (indiceConsumidor+1)%tamanyoCola;
                    return dato;
		}
		return null;
	}
	
}
