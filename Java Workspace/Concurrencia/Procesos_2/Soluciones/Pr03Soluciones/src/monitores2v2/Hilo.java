package monitores2v2;

public class Hilo implements Runnable {
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	
	// ...
	
	final int Ingresar = 0;
	final int Retirar  = 1;
	
	// ...

    private String nombre;
    private Cuenta cuenta;
    
    public Hilo(String nombre, Cuenta cuenta) {
        this.nombre = nombre;
        this.cuenta = cuenta;
    }

	public void run() {
		Integer cantidadFinal;
		
		while (!finalizado) {
			
			int operacion = (int) (Math.random() + 0.5);
			int cantidad = (int) (Math.random() * 100 + 1);

			if (operacion == Ingresar) 
			{
				cantidadFinal = cuenta.ingresar(cantidad);
				System.out.println(this.nombre + " ingresa\n "+ " Saldo inicial: " + (cantidadFinal - cantidad)
						+ "Saldo final: " + cantidadFinal);
			} else /* operacion == Retirar **/
			{
				cantidadFinal = cuenta.retirar(cantidad);
				if (cantidadFinal != null) {
					
					System.out.println(this.nombre + " retira\n "+ " Saldo inicial: " + (cantidadFinal - cantidad)
							+ "Saldo final: " + cantidadFinal);				} else 
								/* false */ 
								System.out.println(this.nombre + ": no hay saldo suficiente (" + cantidad +") euros");
			}

		}

	}
    
}
