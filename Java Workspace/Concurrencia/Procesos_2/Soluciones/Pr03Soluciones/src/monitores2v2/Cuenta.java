package monitores2v2;

public class Cuenta {
	
	private Integer saldo;
	public Cuenta(int saldo) {this.saldo = saldo;}
	
	// ...
	
	public synchronized Integer ingresar(int cantidad) {
		saldo=saldo+cantidad;
		return saldo;
	}
        
	public synchronized Integer retirar(int cantidad) {
		    if (saldo>=cantidad) {saldo=saldo-cantidad;return saldo;}
		    return null;
    }
	
	// ...

	public synchronized int getSaldo() {return saldo;}

}
