package productorConsumidor1;

public class Consumidor implements Runnable {
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	
	Cola cola;
	
	// ...
    
    public Consumidor(Cola cola) {this.cola = cola;}

	public void run() {
		while (!finalizado) {
			int dato = cola.retirar();
			if (dato != Cola.ColaVacia)	{/*Procesar el dato*/}
		}
	}
    
}
