package productorConsumidor1;

public class Cola {
	
	public static final int ColaVacia = -1;
	
	// ...
	
	private int dato;
	private boolean datoAlmacenado = false; 

	
	public synchronized boolean almacenar(int dato) 
	{
		if (!datoAlmacenado) 
		{
		   this.dato = dato;
		   datoAlmacenado = true; /*print para pruebas*/ System.out.println("Almacenado " + dato);
		   return true;
		}
		return false;
	}
	
	
	public synchronized int retirar() 
	{
		if (datoAlmacenado) 
		{
		   datoAlmacenado = false;  /*print para pruebas*/ System.out.println("Retirado " + dato);
		   return dato;
		}
		return ColaVacia;
	}
}
	
	
