package productorConsumidor1;

public class Productor implements Runnable {
	
	final int ValorMinimo = 1; 
	final int ValorMaximo = 10; 
	
	int dato;
	boolean datoEncolado = true;
	Cola cola;
	
	// ...
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	

	
	// ...
    
    public Productor(Cola cola) {
    	this.cola = cola;
    }

	public void run() {

		while (!finalizado) {
			if (datoEncolado) {
				int amplitudRango = ValorMaximo - ValorMinimo + 1;
				int aleatorio = (int) (Math.random() * amplitudRango); // 0..9
				dato = ValorMinimo + aleatorio;
			}
			datoEncolado = cola.almacenar(dato);
		}

	}
    
}
