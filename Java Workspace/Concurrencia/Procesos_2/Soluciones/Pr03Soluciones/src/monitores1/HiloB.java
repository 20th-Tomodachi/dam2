/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monitores1;

/**
 *
 * @author Miguel
 */
public class HiloB implements Runnable{

    private String nombre;
    private Contador contador;
    
    public HiloB (String nombre, Contador contador) {
        this.nombre = nombre;
        this.contador = contador;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            contador.decrementa();
        }
    }
}
