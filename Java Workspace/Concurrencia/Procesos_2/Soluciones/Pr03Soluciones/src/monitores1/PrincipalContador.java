package monitores1;

public class PrincipalContador {

    public static void main(String[] args) {

        Contador cont = new Contador(100);
        HiloA a = new HiloA("HiloA", cont);
        HiloB b = new HiloB("HiloB", cont);

        Thread uno = new Thread(a);
        Thread dos = new Thread(b);

        uno.start();
        dos.start();
    }
}