package monitores2v1;

public class Cola {
	
	private int saldo;
	public Cola(int saldo) {this.saldo = saldo;}
	
	// ...
	
	public synchronized void ingresar(int cantidad) {saldo=saldo+cantidad;}
        
	public synchronized boolean retirar(int cantidad) {
		    if (saldo>=cantidad) {saldo=saldo-cantidad;return true;}
		    return false;
    }
	
	// ...

	public synchronized int getSaldo() {return saldo;}

}
