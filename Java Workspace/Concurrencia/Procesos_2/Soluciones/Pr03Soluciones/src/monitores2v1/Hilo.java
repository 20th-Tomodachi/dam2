package monitores2v1;

public class Hilo implements Runnable {
	
	boolean finalizado = false;
	public void finalizar() {finalizado=true;} 
	
	// ...
	
	final int Ingresar = 0;
	final int Retirar  = 1;
	
	// ...

    private String nombre;
    private Cola cuenta;
    
    public Hilo(String nombre, Cola cuenta) {
        this.nombre = nombre;
        this.cuenta = cuenta;
    }

	public void run() {

		while (!finalizado) {

			int operacion = (int) (Math.random() + 0.5);
			int cantidad = (int) (Math.random() * 100 + 1);

			if (operacion == Ingresar) 
			{
				cuenta.ingresar(cantidad);
				System.out.println(this.nombre + " ingresa " + cantidad + " euros");
			} else /* operacion == Retirar **/
			{
				if (cuenta.retirar(cantidad)) {
					System.out.println(this.nombre + " retira " + cantidad + " euros");
				} else /* false */ System.out.println(this.nombre + ": no hay saldo suficiente (" + cantidad +") euros");
			}

		}

	}
    
}
