package monitores2v1;

public class PrincipalCuenta {

    public static void main(String[] args) {

        Cola cuenta = new Cola(100);
        Hilo hiloJuan = new Hilo("Juan", cuenta);
        Hilo hiloPepe = new Hilo("Pepe", cuenta);

        Thread threadJuan = new Thread(hiloJuan);
        Thread threadPepe = new Thread(hiloPepe);

        threadJuan.start();
        threadPepe.start();
        
     //   hiloJuan.finalizar();
     //   hiloPepe.finalizar();
        
        
        
        
    }
}