package productorConsumidor2;

public class PrincipalProductorConsumidor {

    public static void main(String[] args) {

        Cola cola = new Cola();
        Productor hiloProductor  = new Productor(cola);
        Consumidor hiloConsumidor = new Consumidor(cola);

        Thread threadProductor = new Thread(hiloProductor);
        Thread threadConsumidor = new Thread(hiloConsumidor);

        threadProductor.start();
        threadConsumidor.start();
        
     //   threadProductor.finalizar();
     //   hiloConsumidor.finalizar();
        
        
        
        
    }
}