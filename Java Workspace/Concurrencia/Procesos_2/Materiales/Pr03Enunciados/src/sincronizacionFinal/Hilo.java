package sincronizacionFinal;

public class Hilo implements Runnable {

	private boolean seguir = true;

	// ...

	private String nombreHilo;
	private static int contadorGlobalMensajes = 0;

	// ...

	public Hilo(String nombreHilo) {
		this.nombreHilo = nombreHilo;
	}

	public void run() {
		while (seguir) {
			String mensaje = "";
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 1");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 2");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 3");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 4");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 5");

			imprimirMensaje(mensaje);
		}
	}

	// ...

	public static synchronized void imprimirMensaje(String mensaje) {

		contadorGlobalMensajes++;

		/*
		 * Simulaci�n de procesos del hilo Inicio
		 */
		try {
			Thread.sleep((int) (Math.random() * 100));
		} catch (Exception ex) {
		}
		/*
		 * Simulaci�n de procesos del hilo Fin
		 */

		System.out.println("Mensaje global " + contadorGlobalMensajes);
		System.out.println(mensaje);
	}

	// ...

	public void finalizar() {
		seguir = false;
	}
}
