package estructura2;

public class Hilo implements Runnable {

	public boolean seguir = true;

	@Override
	public void run() {

		System.out.println("Hilo empezado");

		while (seguir) {
			System.out.println("Hilo ejecutandose..");
			try {
				Thread.sleep(100);
			} catch (Exception ex) {
			}
		}
		System.out.println("Hilo acabando..");
	}

}
