package estructura2;

public class Principal {

	public static void main(String[] args) {

		Hilo hilo = new Hilo();
		Thread thread = new Thread(hilo);

		thread.start();

		System.out.println("Hilo principal ejecutándose");
		try {
			Thread.sleep(500);
		} catch (Exception ex) {
		}

		hilo.seguir = false;

		try {
			Thread.sleep(500);
		} catch (Exception ex) {
		}
		System.out.println("Hilo principal acabando...");

	}
}
