package monitores1;

public class HiloB implements Runnable {

	private String nombre;
	private Contador contador;

	public HiloB(String nombre, Contador contador) {
		this.nombre = nombre;
		this.contador = contador;
	}

	@Override
	public void run() {

		for (int i = 0; i <= 100; i++) {
			contador.decrementa();
			System.out.println(contador.getValor());
		}
		
	}

}
