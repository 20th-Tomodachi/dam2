package monitores1;

public class HiloA implements Runnable {

	private String nombre;
	private Contador contador;

	public HiloA(String nombre, Contador contador) {
		this.nombre = nombre;
		this.contador = contador;
	}

	@Override
	public void run() {

		for (int i = 0; i <= 100; i++) {
			contador.incrementa();
			System.out.println(contador.getValor());
		}
		
	}

}
