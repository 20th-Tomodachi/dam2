package sincronizacion1;

public class Hilo implements Runnable {

	private boolean seguir = true;

	// ...

	private String nombreHilo;

	// ...

	public Hilo(String nombreHilo) {
		this.nombreHilo = nombreHilo;
	}

	public void run() {
		while (seguir) {
			System.out.println(nombreHilo + ". Mensaje l�nea 1");
			System.out.println(nombreHilo + ". Mensaje l�nea 2");
			System.out.println(nombreHilo + ". Mensaje l�nea 3");
			System.out.println(nombreHilo + ". Mensaje l�nea 4");
			System.out.println(nombreHilo + ". Mensaje l�nea 5");
		}
	}

	// ...

	public void finalizar() {
		seguir = false;
	}
}
