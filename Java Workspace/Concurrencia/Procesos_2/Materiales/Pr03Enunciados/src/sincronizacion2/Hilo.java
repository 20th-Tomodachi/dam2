package sincronizacion2;

public class Hilo implements Runnable {

	private boolean seguir = true;

	// ...

	private String nombreHilo;

	// ...

	public Hilo(String nombreHilo) {
		this.nombreHilo = nombreHilo;
	}

	public void run() {
		while (seguir) {
			String mensaje = "";
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 1");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 2");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 3");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 4");
			mensaje += "\n" + (nombreHilo + ". Mensaje l�nea 5");

			imprimirMensaje(mensaje);
		}
	}

	public synchronized void imprimirMensaje(String mensaje) {
		System.out.println(mensaje);
	}

	public void finalizar() {
		seguir = false;
	}
}
