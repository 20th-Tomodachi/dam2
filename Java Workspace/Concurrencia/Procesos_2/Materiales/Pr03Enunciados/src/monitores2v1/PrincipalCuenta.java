package monitores2v1;

public class PrincipalCuenta {

	public static void main(String[] args) {

		Cuenta cuenta = new Cuenta(10000);
		Persona p1 = new Persona("Persona1", cuenta);
		Persona p2 = new Persona("Persona2", cuenta);

		Thread tp1 = new Thread(p1);
		Thread tp2 = new Thread(p2);

		tp1.start();
		tp2.start();

		try {
			Thread.sleep(500);
		} catch (Exception ex) {
		}

		p1.finalizar();
		p2.finalizar();
		
	}

}
