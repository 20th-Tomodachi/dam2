package monitores2v1;

public class Cuenta {

	private int saldo;

	public Cuenta(int saldo) {
		this.saldo = saldo;
	}

	public synchronized void incrementar(int cantidad) {
		System.out.println("Saldo actual: " + saldo);
		saldo += cantidad;
		System.out.println("Saldo actualizado: " + saldo);
	}

	public synchronized void decrementar(int cantidad) {
		System.out.println("Saldo actual: " + saldo);
		saldo -= cantidad;
		System.out.println("Saldo actualizado: " + saldo);
	}

}
