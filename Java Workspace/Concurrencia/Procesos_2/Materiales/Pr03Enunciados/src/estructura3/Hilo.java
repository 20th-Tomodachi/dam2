package estructura3;

public class Hilo implements Runnable {

	private boolean seguir = true;

	@Override
	public void run() {

		System.out.println("Hilo empezado");

		while (isSeguir()) {
			System.out.println("Hilo ejecutandose..");
			try {
				Thread.sleep(100);
			} catch (Exception ex) {
			}
		}
		System.out.println("Hilo acabando..");
	}

	public boolean isSeguir() {
		return seguir;
	}

	public void setSeguir(boolean seguir) {
		this.seguir = seguir;
	}

}
