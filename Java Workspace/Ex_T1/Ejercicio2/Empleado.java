import java.util.List;

public class Empleado {
	
	private int id;
	private String nombre;
	private int categoria;
	private List<Periodo> periodos;
	
	public Empleado(int id, String nombre, int categoria, List<Periodo> periodos) {
		this.id = id;
		this.nombre = nombre;
		this.categoria = categoria;
		this.periodos = periodos;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getCategoria() {
		return categoria;
	}
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	public List<Periodo> getPeriodos() {
		return periodos;
	}
	public void setPeriodos(List<Periodo> periodos) {
		this.periodos = periodos;
	}
	
}
