import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

public class Principal {

	public static void main(String[] args) {

		// TODO Probar el orden del sort.
		
	}

	public static ArrayList<Empleado> seleccion(ArrayList<Empleado> empleados) {

		ArrayList<Empleado> nuevaLista = new ArrayList<>();

		for (Empleado e : empleados) {
			if (e.getCategoria() == 2) {
				nuevaLista.add(e);
			}
		}

		// Quiz� funcione solo sumando ambos GregorianCalendar, de cada Empleado, y comparandolos.
		nuevaLista.sort(new Comparator<Empleado>() {
			@Override
			public int compare(Empleado e1, Empleado e2) {
				return (int) diasDisfrutados(e1) - (int) diasDisfrutados(e2);
			}
		});
		return nuevaLista;
	}

	public static long diasDisfrutados(Empleado empleado) {
		long diasDisfrutados = 0;
		for (Periodo p : empleado.getPeriodos()) {
			Date dateInicio = p.getFechaInicio().getTime();
			Date dateFin = p.getFechaFin().getTime();

			diasDisfrutados = diasDisfrutados + (dateInicio.getTime() - dateFin.getTime() / (1000 * 60 * 60 * 24));
		}
		return diasDisfrutados;
	}

}