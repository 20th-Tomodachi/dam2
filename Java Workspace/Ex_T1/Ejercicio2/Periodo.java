import java.util.GregorianCalendar;

public class Periodo {

	private GregorianCalendar fechaInicio;
	private GregorianCalendar fechaFin;
	private Empleado empleado;
	
	public Periodo(GregorianCalendar fechaInicio, GregorianCalendar fechaFin, Empleado empleado) {
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.empleado = empleado;
	}
	
	public GregorianCalendar getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(GregorianCalendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public GregorianCalendar getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(GregorianCalendar fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

}
