import java.util.ArrayList;

public class Componente {

	private String id;
	ArrayList<Proveedor> listaProveedor;
	
	public Componente(String id, ArrayList<Proveedor> listaProveedor) {
		super();
		this.id = id;
		this.listaProveedor = listaProveedor;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<Proveedor> getListaProveedor() {
		return listaProveedor;
	}
	public void setListaProveedor(ArrayList<Proveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}
	
	
	
}
