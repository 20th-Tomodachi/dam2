import java.util.List;

public class Pieza {

	private String id;
	List<Componente> componentes;
	private int numComponentes;
	
	public Pieza(String id, List<Componente> componentes, int numComponentes) {
		super();
		this.id = id;
		this.componentes = componentes;
		this.numComponentes = numComponentes;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Componente> getComponentes() {
		return componentes;
	}
	public void setComponentes(List<Componente> componentes) {
		this.componentes = componentes;
	}
	public int getNumComponentes() {
		return numComponentes;
	}
	public void setNumComponentes(int numComponentes) {
		this.numComponentes = numComponentes;
	}
	
	
	
	
}
