import java.util.List;

public class Proveedor {

	private String id;
	private String pais;
	List<Componente> componentes;
	
	public Proveedor(String id, String pais, List<Componente> componentes) {
		super();
		this.id = id;
		this.pais = pais;
		this.componentes = componentes;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public List<Componente> getComponentes() {
		return componentes;
	}
	public void setComponentes(List<Componente> componentes) {
		this.componentes = componentes;
	}
}
