import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

public class Principal {

	public static void main(String[] args) {
		ArrayList<Opositor> opositores = new ArrayList<>();
		ArrayList<Opositor> descartados = new ArrayList<>();
		Opositor op;

		for (int i = 1; i <= 40; i++) {
			op = new Opositor("Pepe" + i, "Pepito" + i, "Pepon A" + i, LocalDate.of(2020, 9, 20));
			opositores.add(op);
			if (i >= 35) {
				Opositor opNew = new Opositor("Pepe" + i, "Pepito" + i, "Pepon A" + i, LocalDate.of(2020, 9, 10));
				opositores.add(opNew);
				descartados.add(op);
			}
		}
		opositores.add(op = new Opositor("Ana", "Pepito1", "Anton", LocalDate.of(2020, 8, 1)));

		System.out.println(solicitantes(opositores, descartados));
		System.out.println();
		System.out.println(ordenar(opositores));
	}

	public static ArrayList<Opositor> solicitantes(ArrayList<Opositor> opositores, ArrayList<Opositor> descartados) {

		ArrayList<Opositor> op = new ArrayList<>(opositores);
		ArrayList<Opositor> opDesc = new ArrayList<>(descartados);

		for (Opositor o : opositores) {
			if (opDesc.contains(o))
				op.remove(o);
		}

		if (op.size() > 30) {
			op.sort(new Comparator<Opositor>() {
				@Override
				public int compare(Opositor op1, Opositor op2) {
					return (op1.getFechaSolicitud().compareTo(op2.getFechaSolicitud()));
				}
			});
		}
		return op;
	}

	public static ArrayList<Opositor> ordenar(ArrayList<Opositor> opositores) {
		ArrayList<Opositor> op = new ArrayList<>(opositores);
		op.sort(new Comparator<Opositor>() {
			@Override
			public int compare(Opositor op1, Opositor op2) {
				int result = (op1.getApellido1().compareToIgnoreCase(op2.getApellido1()));
				if (result == 0)
					return op1.getApellido2().compareToIgnoreCase(op2.getApellido2());
				return result;
			}
		});
		return op;
	}

}
