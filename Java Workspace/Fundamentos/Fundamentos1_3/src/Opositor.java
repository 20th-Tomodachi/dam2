import java.time.LocalDate;

public class Opositor {

	private String apellido1;
	private String apellido2;
	private String nombre;
	private LocalDate fechaSolicitud;

	public Opositor(String nombre, String apellido1, String apellido2, LocalDate fechaSolicitud) {
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getApellido1() {
		return apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public String getNombre() {
		return nombre;
	}

	public LocalDate getFechaSolicitud() {
		return fechaSolicitud;
	}

	@Override
	public String toString() {
		return "Nombre: " + nombre + ", Apellido1: " + apellido1 + ", Apellido2: " + apellido2 + ", FechaSolicitud: "
				+ fechaSolicitud + "]\n";
	}

}
