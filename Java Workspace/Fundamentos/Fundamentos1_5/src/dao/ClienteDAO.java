package dao;

import java.util.ArrayList;
import java.util.List;

import dominio.Cliente;

public class ClienteDAO implements ClienteDAOInterface {

	private List<Cliente> lista = new ArrayList<>();;

	public List<Cliente> getLista() {
		return lista;
	}

	@Override
	public boolean alta(Cliente c) {
		for (Cliente cliente : this.lista)
			if (cliente.getIdCliente().equalsIgnoreCase(c.getIdCliente()))
				return false;
		return this.lista.add(c);
	}

	@Override
	public void baja(String idCliente) {
		for (Cliente cliente : this.lista)
			if (cliente.getIdCliente().equalsIgnoreCase(idCliente))
				this.lista.remove(cliente);
	}

	// TODO Que modifica?
	@Override
	public void modificacion(Cliente c) {

	}

	@Override
	public Cliente consulta(String idCliente) {
		for (Cliente cliente : this.lista)
			if (cliente.getIdCliente().equalsIgnoreCase(idCliente))
				return cliente;
		return null;
	}

	@Override
	public List<Cliente> consulta() {
		try {
			List<Cliente> listNueva = new ArrayList<Cliente>(this.lista);
			return listNueva;
		} catch (Exception e) {
			return null;
		}
	}

}
