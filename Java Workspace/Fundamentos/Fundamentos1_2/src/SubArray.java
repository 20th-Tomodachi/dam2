import java.util.Arrays;

public class SubArray {

	public static void main(String[] args) {

		int[] arr1 = { 8, 2, 3, 4, 5, 6, 7 };
		int[] arr2 = { 1, 2, 3, 4, 8, 6, 7 };
		int[] arr3 = { 1, 2, 3, 8, 5, 6, 7 };

		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr2));
		System.out.println(Arrays.toString(arr3));
		System.out.println();
		System.out.println(Arrays.toString(extraer(arr1)));
		System.out.println(Arrays.toString(extraer(arr2)));
		System.out.println(Arrays.toString(extraer(arr3)));
	}

	public static int[] extraer(int[] datos) {

		boolean encontrado = false;
		boolean quedanElementos = true;
		int[] resultado = new int[4];

		if (datos == null)
			return null;
		int i = 0;
		do {
			if (datos[i] == 8)
				encontrado = true;
			if (i < datos.length - 4)
				quedanElementos = false;

			i++;
		} while (!encontrado && !quedanElementos);
		if (!encontrado)
			return null;
		try {
			for (int j = 0; j < 4; j++) {
				resultado[j] = datos[j + i - 1];
			}
		} catch (Exception ex) {
			return null;
		}
		return resultado;
	}
}
