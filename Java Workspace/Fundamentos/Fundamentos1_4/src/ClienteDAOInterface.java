import java.util.ArrayList;

public interface ClienteDAOInterface {

	public boolean altaCliente(Cliente cliente);

	public boolean bajaCliente(Cliente cliente);

	public Cliente consultaID(int id);

	public ArrayList<Cliente> consulta();

}
