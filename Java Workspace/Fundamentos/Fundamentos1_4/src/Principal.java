
public class Principal {

	public static void main(String[] args) {

		BaseClientes b1 = new BaseClientes();

		b1.getBaseClientes().getLista().add(new Cliente("Juan", "Perico"));
		b1.getBaseClientes().getLista().add(new Cliente("Juan2", "Perico2"));
		b1.getBaseClientes().getLista().add(new Cliente("Juan3", "Perico3"));

		for (Cliente c : b1.getBaseClientes().getLista()) {
			System.out.println(b1.getBaseClientes().altaCliente(c));
			System.out.println(b1.getBaseClientes().bajaCliente(c));
			System.out.println(b1.getBaseClientes().consultaID(2));
			System.out.println(b1.getBaseClientes().consulta());
			System.out.println();
		}

	}
}
