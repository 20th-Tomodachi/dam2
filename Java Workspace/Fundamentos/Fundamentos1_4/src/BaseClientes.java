public class BaseClientes {

	private ClienteDAO baseClientes;

	public ClienteDAO getBaseClientes() {
		return baseClientes;
	}

	public void setBaseClientes(ClienteDAO baseClientes) {
		this.baseClientes = baseClientes;
	}

	public BaseClientes() {
		this.baseClientes = new ClienteDAO();
	}

	/**
	 * Copia la lista de una BaseClientes dada en una nueva BaseCliente.
	 * 
	 * @param baseClientes La BaseClientes a copiar.
	 */
	public BaseClientes(BaseClientes baseClientes) {
		for (Cliente c : baseClientes.getBaseClientes().getLista())
			this.baseClientes.getLista().add(c);
	}

}
