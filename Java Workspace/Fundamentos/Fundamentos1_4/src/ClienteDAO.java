import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements ClienteDAOInterface {

	private List<Cliente> lista = new ArrayList<>();

	public List<Cliente> getLista() {
		return lista;
	}

	public boolean altaCliente(Cliente cliente) {
		if (cliente.isEstadoCliente() == true)
			return true;
		try {
			cliente.setEstadoCliente(true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean bajaCliente(Cliente cliente) {
		if (cliente.isEstadoCliente() == false)
			return true;
		try {
			cliente.setEstadoCliente(false);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Cliente consultaID(int id) {
		for (Cliente c : this.lista)
			if (c.getIdCliente() == id)
				return c;
		return null;
	}

	public ArrayList<Cliente> consulta() {
		ArrayList<Cliente> listaNueva = (ArrayList<Cliente>) this.lista;
		return listaNueva;
	}

}
