public class Cliente {

	private static int siguienteIdCliente = 1;

	private String nombre;
	private String apellido;
	private final int idCliente;
	private boolean estadoCliente;

	// TODO Moverlo a BaseClientes
	private int getSiguienteIdCliente() {
		int id = siguienteIdCliente;
		siguienteIdCliente++;
		return id;
	}

	public Cliente(String nombre, String apellido) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.idCliente = getSiguienteIdCliente();
	}

	public boolean isEstadoCliente() {
		return estadoCliente;
	}

	public void setEstadoCliente(boolean estadoCliente) {
		this.estadoCliente = estadoCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getIdCliente() {
		return idCliente;
	}

	@Override
	public String toString() {
		return "Cliente Numero: " + idCliente + ", Nombre: " + nombre + ", Apellido: " + apellido + ", Estado: "
				+ estadoCliente + ".";
	}

}
