public class Main {

	public static void main(String[] args) {
		char[][] mPequenya = { { 'b', 'g' }, { 'f', 'c' }, { 'j', 'k' } };
		char[][] mGrande = { { 'a', 'b', 'g' }, { 'c', 'f', 'c' }, { 'h', 'j', 'k' } };

		System.out.println(matrizContenida(mGrande, mPequenya));
	}

	private static boolean matrizContenida(char[][] mGrande, char[][] mPequenya) {

		for (int i = 0; i < mGrande.length; i++)
			for (int j = 0; j < mGrande[0].length; j++)
				if (mPequenyaEn(i, j, mGrande, mPequenya))
					return true;

		return false;
	}

	private static boolean mPequenyaEn(int i, int j, char[][] mGrande, char[][] mPequenya) {

		for (int k = 0; k < mPequenya.length; k++)
			for (int m = 0; m < mPequenya[0].length; m++)
				try {
					if (mPequenya[k][m] != mGrande[i + k][j + m])
						return false;
				} catch (Exception ex) {
					return false;
				}

		return true;
	}

}
