
public class Rey extends Pieza {

	public Rey(Color color, int x, int y) {
		super(color, x, y);
		this.setNombrePieza("R" + this.getColor().ordinal());
		setEstaVacio(false);
	}

	@Override
	public boolean mover(Casilla[][] casillas, int x, int y) {
		String[] movimientos = movimientosPosibles(casillas, x, y).split(",");

		try {

			for (String s : movimientos) {
				if (s.equalsIgnoreCase((Integer.toString(x) + Integer.toString(y))))
					if (casillas[x][y].isEstaVacio()) {
						this.setX(x);
						this.setY(y);
						return true;
						// TODO tirar excepcion para arribar diciendo que ha comido una ficha
					} else if (this.getColor().ordinal() != ((Pieza) casillas[x][y]).getColor().ordinal()) {
						this.setX(x);
						this.setY(y);
						return true;
					}
			}

			// TODO ver excepciones, si tratarlo aqui o en el metodo que lo llama.
		} catch (IndexOutOfBoundsException e) {

		}
		return false;
	}

	// Metodo para sacar un String con todos los movimientos posibles
	@Override
	public String movimientosPosibles(Casilla casillas[][], int xDestino, int yDestino) {
		String result = "";

		int moviX = this.getX();
		int moviY = this.getY();
		// Probar String.contains(movimientos) para el for-each
		for (int i = moviX; i < moviX + 1 && i > moviX - 1; i++) {
			for (int j = moviY; j < moviY + 1 && j > moviY - 1; j++) {
				result = result + i + j + ",";
			}
		}
		return result;
	}

	// toString para poder visualizar la pieza en el tablero
	@Override
	public String toString() {
		return " " + this.getNombrePieza();
	}

}
