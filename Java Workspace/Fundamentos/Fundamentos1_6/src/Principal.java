public class Principal {

	public static void main(String[] args) {

		Tablero t1 = new Tablero();

		Casilla tablero[][] = t1.getCasillas();

		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				tablero[i][j] = new Casilla(i, j) {
				};
			}
		}

		tablero[0][0] = new Rey(Color.Blanco, 0, 0);

		System.out.println(mostrarTablero(tablero));

	}

	public static String mostrarTablero(Casilla[][] tablero) {
		String result = "";

		for (int i = 0; i < tablero.length; i++) {
			result = result + (i + 1) + " |";
			for (int j = 0; j < tablero[i].length; j++) {
				result = result + (tablero[i][j]);
			}
			result += "\n";
		}

		return result;
	}

	// TODO para poder generar movimientos aleatorios, usar el metodo
	// "movimientosPosibles" de cada pieza, con un math.Random sacar un (despues de
	// hacer Split al String) uno de los posibles movimientos en 2 integer, que
	// serian las coordenadasDestino a mover, y llamar al metodo mover de la pieza.

}
