
public class Tablero {

	Casilla[][] casillas = new Casilla[8][8];

	public Casilla[][] getCasillas() {
		return casillas;
	}

	public void setCasillas(Casilla[][] casillas) {
		this.casillas = casillas;
	}

	// TODO Metodo auxiliar por desarrollar
	public Pieza getPiezaPosicion(int x, int y) {

		// TODO cambiar null
		return null;
	}

	public Pieza moverPieza(Pieza pieza, int xDestino, int yDestino) {

		int xPieza = pieza.getX();
		int yPieza = pieza.getY();

		// Cambia las coordenadas del objeto Pieza pero no toca nada de casillas[][]
		pieza.mover(casillas, xDestino, yDestino);

		// TODO mover cosas de casillas
	
		// TODO cambiar null
		return null;
	}
}
