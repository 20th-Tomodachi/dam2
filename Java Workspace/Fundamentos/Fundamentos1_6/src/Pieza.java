
public abstract class Pieza extends Casilla {

	private Color color;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Pieza(Color color, int x, int y) {
		super(x, y);
		this.color = color;
	}

	public abstract boolean mover(Casilla[][] casillas, int x, int y);

	public abstract String movimientosPosibles(Casilla[][] casillas, int x, int y);
}
