
public abstract class Casilla {

	private boolean estaVacio = true;
	private String nombrePieza;
	private int x;
	private int y;

	public Casilla(int x, int y) {
		this.nombrePieza = "x ";
		this.x = x;
		this.y = y;
	}

	public boolean isEstaVacio() {
		return estaVacio;
	}

	public void setEstaVacio(boolean estaVacio) {
		this.estaVacio = estaVacio;
	}

	public String getNombrePieza() {
		return nombrePieza;
	}

	public void setNombrePieza(String nombrePieza) {
		this.nombrePieza = nombrePieza;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	// toString para poder visualizar las casillas libres en el tablero
	@Override
	public String toString() {
		return " " + this.nombrePieza;
	}

}
