package pfc.clientes.principal;

import java.util.GregorianCalendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pfc.clientes.dao.ClienteDAOInterface;
import pfc.clientes.dominio.ClienteB;
import pfc.clientes.dominio.Servicio;

public class Principal {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ClienteDAOInterface clientes = (ClienteDAOInterface) context.getBean("clienteDAO");
		
		ClienteB cliente;
		
		ClienteB cliente1 = new ClienteB("juan", "juan@gmail.com", "Murcia");
		ClienteB cliente2 = new ClienteB("pedro", "pedro@gmail.com", "Madrid");
		ClienteB cliente3 = new ClienteB("luis", "luis@gmail.com", "Canarias");
		
		// Borramos todo, para no tener que ejecutar el script de la base cada vez
		for (ClienteB c :clientes.consultaAll()) {
			clientes.baja(c.getIdClienteB());
		}
		
		//Alta: FUNCIONA
		System.out.println();
		System.out.println("Alta de clientes en proceso");		
		clientes.alta(cliente3);
		clientes.alta(cliente2);
		clientes.alta(cliente1);
		
		System.out.println();
		System.out.println("Lista de clientes");
		for (ClienteB c : clientes.consultaAll()) {
			System.out.println(c.getIdClienteB() + "\t" + c.getNombre() + "\t\t" + c.getEmail() + "\t\t" + c.getProvincia());
			System.out.flush();
		}

		
		GregorianCalendar calendar1 = new GregorianCalendar(2018, 6, 27, 16, 16, 47);
		GregorianCalendar calendar2 = new GregorianCalendar(2019, 12, 27, 16, 16, 47);
		
		Servicio s3 = new Servicio(cliente3, calendar1, 40.5, "Lavar pies");
		Servicio s2 = new Servicio(cliente2, calendar2, 12.3, "Lavar ropa");
		
		//Asignar servicios: LOS DOS SE ASIGNAN A JUAN
		System.out.println();
		System.out.println("Asignando servicios");		
		clientes.crearServicio(s3);
        clientes.crearServicio(s2);
//		
//		System.out.println();
//		//Mostrar servicio de Luis: ERROR
//		System.out.println("Lista de servicios:");
//		System.out.println(clientes.consultaAllServicios());
		
		 
		 //RECORRER SERVICIOS Y MOSTRARLOS
//		 System.out.println();
//		 System.out.println("Lista de servicios");
//		 for (Servicio s : clientes.consultaAllServicios())
//		 System.out.println(s.getIdServicio() + "\t" + (s.getCliente().getIdClienteB()) + "\t\t" + s.getFecha() + "\t\t" + s.getImporte() + "\t\t" + s.getDescripcion());
//		 System.out.flush();
		
	}
}
