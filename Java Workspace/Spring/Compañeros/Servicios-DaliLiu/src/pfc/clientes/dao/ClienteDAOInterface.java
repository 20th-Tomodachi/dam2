package pfc.clientes.dao;

import java.util.List;

import pfc.clientes.dominio.ClienteB;
import pfc.clientes.dominio.Servicio;

public interface ClienteDAOInterface {
	
	public boolean alta(ClienteB c);
	public void baja(int idCliente);
	public void modificacion(ClienteB c);
	public ClienteB consulta(int idCliente);
	public List<ClienteB> consultaAll();
	public boolean crearServicio(Servicio s);
	public Servicio consultaServicio(int idServicio);
	public List<Servicio> consultaAllServicios();
}
