package pfc.clientes.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pfc.clientes.dominio.ClienteB;
import pfc.clientes.dominio.Servicio;

@Repository
@Transactional
public class ClienteDAOSpringJdbc implements ClienteDAOInterface {

	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate;}

	public ClienteDAOSpringJdbc() {
	}

	@Override
	public boolean alta(ClienteB c) {
		try 
		{
		 //jdbcTemplate.setQueryTimeout(5);
		 String SQL = "INSERT INTO TClientes2 VALUES (?, ?, ?, ?)";
		 jdbcTemplate.update(SQL, c.getIdClienteB(), c.getNombre(),	c.getEmail(), 
					         c.getProvincia());
			
		} catch ( DataAccessException ex ) {return false;}

		return true;
	}

	@Override
	public void baja(int idClienteB) {
		String SQL = "DELETE FROM TClientes2 WHERE idCliente = ?";
		jdbcTemplate.update(SQL, idClienteB);
		
	};

	@Override
	public void modificacion(ClienteB c) {
		String SQL = "UPDATE TClientes2 SET nombre = ?, email= ?, provincia= ? WHERE idCliente = ?";
		jdbcTemplate.update(SQL, c.getNombre(),	c.getEmail(), 
	            c.getProvincia(), c.getIdClienteB());
		
	}
	
	@Override
	public ClienteB consulta(int idCliente) {
		String SQL = "SELECT * FROM TClientes2 WHERE idCliente = ?";
		ClienteB cliente = jdbcTemplate.queryForObject(SQL,new Object[] { idCliente }, new ClienteMapper());
		return cliente;
	}

	
	@Override
	public List<ClienteB> consultaAll() {
		String SQL = "SELECT * FROM TClientes2";
		List<ClienteB> todoslosClientes = jdbcTemplate.query(SQL, new ClienteMapper());
		return todoslosClientes;
	}

	@Override
	public boolean crearServicio(Servicio s) {
		try {
			Date fechaServicio = s.getFecha().getGregorianChange();
			String SQL = "INSERT INTO TServicios2 VALUES (?, ?, ?, ?, ?)";
			jdbcTemplate.update(SQL, s.getIdServicio(), (s.getCliente().getIdClienteB()), fechaServicio, s.getImporte(), 
					            s.getDescripcion());
		} catch (DataAccessException ex) {return false;}
		return true;
	}

	@Override
	public List<Servicio> consultaAllServicios() {
		List<Servicio> todoslosServicios;
		try {
			String SQL = "SELECT * FROM TServicios2";
			todoslosServicios = jdbcTemplate.query(SQL, new ServicioMapper());
		} catch (DataAccessException ex) {ex.printStackTrace(); todoslosServicios = null;}
		return todoslosServicios;
	}

	@Override
	public Servicio consultaServicio(int idServicio) {
		String SQL = "SELECT * FROM TServicios2 WHERE idServicio = ?";
		Servicio servicio = jdbcTemplate.queryForObject(SQL,new Object[] { idServicio }, new ServicioMapper());
		return servicio;
	}

}



class ClienteMapper implements RowMapper<ClienteB> {

	public ClienteB mapRow(ResultSet rs, int rowNum) throws SQLException 
	{
		ClienteB cliente = new ClienteB();
		cliente.setIdClienteB(rs.getInt("idCliente"));
		cliente.setNombre(rs.getString("nombre"));
		cliente.setEmail(rs.getString("email"));
		cliente.setProvincia(rs.getString("provincia"));
		return cliente;
	}
	
}


class ServicioMapper implements RowMapper<Servicio> {
	
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate;}
	
//	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//	ClienteDAOInterface clientes = (ClienteDAOInterface) context.getBean("clienteDAO");

	public Servicio mapRow(ResultSet rs, int rowNum) throws SQLException 
	{	
		Servicio servicio = new Servicio();
		servicio.setIdServicio(rs.getInt("idServicio"));
		
		int id= rs.getInt("idCliente");
		String SQL = "SELECT * FROM TClientes2 WHERE idCliente = ?";
		ClienteB cliente = jdbcTemplate.queryForObject(SQL,new Object[] { id }, new ClienteMapper());
		servicio.setCliente(cliente);
		//servicio.setCliente(clientes.consulta(id));
		
		Date date = rs.getDate("fecha");
		GregorianCalendar cal = new GregorianCalendar();
	    cal.setTime(date);
		servicio.setFecha(cal);
		
		servicio.setImporte(rs.getDouble("importe"));
		servicio.setDescripcion(rs.getString("descripcion"));
		return servicio;
	}
	
}


