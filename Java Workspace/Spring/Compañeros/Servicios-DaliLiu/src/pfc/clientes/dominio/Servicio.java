package pfc.clientes.dominio;

import java.sql.Date;
import java.util.GregorianCalendar;

public class Servicio {
private int idServicio;
private String descripcion;
private GregorianCalendar fecha;
private double importe;
private ClienteB cliente;


public Servicio(ClienteB cliente, GregorianCalendar fecha, 
		        double importe, String descripcion) {
	
	this.descripcion = descripcion;
	this.fecha = fecha;
	this.importe = importe;
	this.cliente = cliente;
}
public Servicio(){}


public int getIdServicio() {
	return idServicio;
}
public void setIdServicio(int idServicio) {
	this.idServicio = idServicio;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public GregorianCalendar getFecha() {
	return fecha;
}

public double getImporte() {
	return importe;
}
public void setImporte(double importe) {
	this.importe = importe;
}
public ClienteB getCliente() {
	return cliente;
}
public void setCliente(ClienteB cliente) {
	this.cliente = cliente;
}
public void setFecha(GregorianCalendar fecha) {
	this.fecha = fecha;
}

}
