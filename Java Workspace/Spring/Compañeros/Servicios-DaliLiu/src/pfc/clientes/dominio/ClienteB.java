package pfc.clientes.dominio;

import java.util.List;

public class ClienteB {

		private int  idCliente;
		private String provincia;
		private String email;
		private String nombre;
		private List<Servicio>servicios;
		
		public ClienteB(String nombre, String email, String provincia) {

			this.email=email;
			this.provincia = provincia;
			this.nombre = nombre;
		}
		public ClienteB() {

		}
		public int getIdClienteB() {
			return idCliente;
		}
		public void setIdClienteB(int idCliente) {
			this.idCliente = idCliente;
		}
		public String getProvincia() {
			return provincia;
		}
		public void setProvincia(String provincia) {
			this.provincia = provincia;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public List<Servicio> getServicios() {
			return servicios;
		}
		public void setServicios(List<Servicio> servicios) {
			this.servicios = servicios;
		}
	}

