package principal;

import java.time.Month;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.ClienteDAOInterface;
import dao.ServicioDAOInterface;
import dominio.Cliente;
import dominio.Servicio;

public class Principal {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ClienteDAOInterface clientes = (ClienteDAOInterface) context.getBean("clienteDAO");
		ServicioDAOInterface servicios = (ServicioDAOInterface) context.getBean("servicioDAO");

		Cliente cliente;

		Cliente cliente1 = new Cliente("juan", "pwjuan", "Juan Alcaraz");
		Cliente cliente2 = new Cliente("pedro", "pwpedro", "Pedro Zamora");
		Cliente cliente3 = new Cliente("luis", "pwluis", "Luis Valera");
		
		Servicio S;
		Date fechaSer1 = new Date(2020,2,15);
		Servicio ser1 = new Servicio("Reparación tubiería", fechaSer1, 75.0, cliente3);
		Date fechaSer2 = new Date(2019, 5, 13);
		Servicio ser2 = new Servicio("Pintar pared", fechaSer2, 100.0, cliente1);
		Date fechaSer3 = new Date (2020, 6, 15);
		Servicio ser3 = new Servicio("Reglaje general", fechaSer3, 150.0, cliente2);
		
		// Borramos todo, para no tener que ejecutar el script de la base cada vez
				for (Cliente c : clientes.consultarTodos())
					clientes.baja(c.getIdCliente());
				
				for(Servicio s : servicios.consultarTodos())
					servicios.baja(s.getIdServicio());



		System.out.println();
		System.out.println("Alta de clientes en proceso");
		clientes.alta(cliente1);
		clientes.alta(cliente2);
		clientes.alta(cliente3);
		
		System.out.println();
		System.out.println("Alta de servicios en proceso");
		servicios.alta(ser1);
		servicios.alta(ser2);
		servicios.alta(ser3);

		System.out.println();
		System.out.println("Lista de clientes");
		for (Cliente c : clientes.consultarTodos())
			System.out.println(c.getIdCliente() + "\t" + c.getNombre() + "\t" + c.getEmail() + "\t" + c.getProvincia() + "\t" + c.getServicios());
		
		System.out.println();
		System.out.println("Lista de servicios");
		for (Servicio s : servicios.consultarTodos())
			System.out.println(s.getIdServicio() + "\t" + s.getDescripcion() + "\t" + s.getFecha() + "\t"  + s.getImporte() + "\t" + s.getCliente());

		System.out.println();
		System.out.println("Baja de " + cliente1.getIdCliente() + " en proceso");
		clientes.baja(cliente1.getIdCliente());
		
		System.out.println();
		System.out.println("Baja de servicio con id " + ser1.getIdServicio() + " en proceso");
		servicios.baja(ser1.getIdServicio());

		System.out.println();
		System.out.println("Lista de clientes");
		for (Cliente c : clientes.consultarTodos())
			System.out.println(c.getIdCliente() + "\t" + c.getNombre() + "\t" + c.getEmail() + "\t" + c.getProvincia() + "\t" + c.getServicios());
		
		System.out.println();
		System.out.println("Lista de servicios");
		for (Servicio s : servicios.consultarTodos())
			System.out.println(s.getIdServicio() + "\t" + s.getDescripcion() + "\t" + s.getFecha() + "\t"  + s.getImporte() + "\t" + s.getCliente());

		System.out.println();
		System.out.println("Modificación de " + cliente2.getIdCliente() + " en proceso");
		cliente = clientes.consulta(cliente2.getIdCliente());
		cliente.setNombre("Pedro Zamora Zamora");
		clientes.modificacion(cliente);

		System.out.println();
		System.out.println("Lista de clientes");
		for (Cliente c : clientes.consultarTodos())
			System.out.println(c.getIdCliente() + "\t" + c.getNombre() + "\t" + c.getEmail() + "\t" + c.getProvincia() + "\t" + c.getServicios());
		
		System.out.println();
		System.out.println("Lista de servicios");
		for (Servicio s : servicios.consultarTodos())
			System.out.println(s.getIdServicio() + "\t" + s.getDescripcion() + "\t" + s.getFecha() + "\t"  + s.getImporte() + "\t" + s.getCliente());

	}
}
