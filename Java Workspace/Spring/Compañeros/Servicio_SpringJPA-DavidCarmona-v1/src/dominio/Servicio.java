package dominio;

import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tservicios")
public class Servicio {

	@Id
	private int idServicio;
	private String descripcion;
	private GregorianCalendar fecha;
	private double importe;
	private Cliente cliente;

	public Servicio(String descripcion, GregorianCalendar fecha, double importe, Cliente cliente) {
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.importe = importe;
		this.cliente = cliente;
	}

	public Servicio() {
	}

// G and S
	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public GregorianCalendar getFecha() {
		return fecha;
	}

	public void setFecha(GregorianCalendar fecha) {
		this.fecha = fecha;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
