package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dominio.Servicio;

@Repository
@Transactional
public class ServicioDAO_SpringJPA implements ServicioDAOInterface {

	private EntityManager em;

	@Override
	public boolean alta(Servicio s) {
		try {
			em.persist(s);

		} catch (DataAccessException ex) {
			return false;
		}

		return true;
	}

	@Override
	public void baja(int idServicio) {
		Servicio s = this.consulta(idServicio);
		em.remove(s);
	}

	@Override
	public void modificacion(Servicio s) {
		em.merge(s);
	}

	@Override
	@Transactional(readOnly = true)
	public Servicio consulta(int idServicio) {
		return em.find(Servicio.class, idServicio);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Servicio> consultarTodos() {
		Query pregunta = em.createQuery("SELECT s FROM s");
		List<Servicio> todoslosServicios = pregunta.getResultList();
		return todoslosServicios;
	}
}
