package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dominio.Cliente;

@Repository
@Transactional
public class ClienteDAO_SpringJPA implements ClienteDAOInterface {

	private EntityManager em;
	
	@Override
	public boolean alta(Cliente c) {
		try {
			em.persist(c);
		} catch (DataAccessException ex) {
			return false;
		}

		return true;
	}

	@Override
	public void baja(int idCliente) {
		Cliente c = this.consulta(idCliente);
		em.remove(c);
	}

	@Override
	public void modificacion(Cliente c) {
		em.merge(c);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente consulta(int idCliente) {
		return em.find(Cliente.class, idCliente);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> consultarTodos() {
		Query pregunta = em.createQuery("SELECT c FROM Cliente c");
		List<Cliente> todosLosClientes = pregunta.getResultList();
		return todosLosClientes;
	}
}
