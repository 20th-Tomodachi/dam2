package com.rubencarmona.myteacher.domain;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the teacher database table.
 * 
 */
@Entity
@NamedQuery(name="Teacher.findAll", query="SELECT t FROM Teacher t")
public class Teacher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int teacherid;

	@Column
	private String name;

	@Column
	private String surname;
	
	@Column
	private byte enabled;

	public Teacher() {
	}
	
	public Teacher(int teacherid, String name, String surname) {
		super();
		this.teacherid = teacherid;
		this.name = name;
		this.surname = surname;
		this.enabled=1;
		
	}
	
	public Teacher(String name, String surname) {
		super();
		this.enabled=1;
		this.name = name;
		this.surname = surname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + teacherid;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (teacherid != other.teacherid)
			return false;
		return true;
	}

	public byte getEnabled() {
		return enabled;
	}



	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}



	public int getTeacherid() {
		return this.teacherid;
	}

	public void setTeacherid(int teacherid) {
		this.teacherid = teacherid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "Teacher [teacherid=" + teacherid + ", name=" + name + ", surname=" + surname + ", enabled=" + enabled
				+ "]";
	}
	
	
	
	


}