package com.rubencarmona.myteacher.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name="authorities_users")
public class AuthoritiesUsers {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	private long userid;
	
	@Column
	private long authorityid;
	
	

	public AuthoritiesUsers() {
		super();		
	}

	public AuthoritiesUsers(long userid, long authorityid) {
		super();
		this.userid = userid;
		this.authorityid = authorityid;
	}
	
	public AuthoritiesUsers(long id, long userid, long authorityid) {
		super();
		this.id = id;
		this.userid = userid;
		this.authorityid = authorityid;
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}

	public long getAuthorityid() {
		return authorityid;
	}

	public void setAuthorityid(long authorityid) {
		this.authorityid = authorityid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorityid ^ (authorityid >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (userid ^ (userid >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthoritiesUsers other = (AuthoritiesUsers) obj;
		if (authorityid != other.authorityid)
			return false;
		if (id != other.id)
			return false;
		if (userid != other.userid)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthoritiesUsers [id=" + id + ", userid=" + userid + ", authorityid=" + authorityid + "]";
	}
	
	
	
	
	
	

}
