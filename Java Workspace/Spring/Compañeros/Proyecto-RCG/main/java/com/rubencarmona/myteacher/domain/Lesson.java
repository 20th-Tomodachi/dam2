package com.rubencarmona.myteacher.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import java.util.Date;


/**
 * The persistent class for the lesson database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@NamedQuery(name="Lesson.findAll", query="SELECT l FROM Lesson l")
public class Lesson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idlesson;

	@Column
	private int assessment;

	@Column
	private Date date;

	
	@Column(name="teacherid")
	private int teacher;

	
	@Column(name="userid")
	private int user;

	public Lesson() {
	}
	
	public Lesson(Teacher teacher, User user, Date date) {
		super();
		this.assessment = 0;
		this.date = date;
		this.teacher = teacher.getTeacherid();
		this.user = user.getUserid();
	}
	
	

	public int getIdlesson() {
		return this.idlesson;
	}

	public void setIdlesson(int idlesson) {
		this.idlesson = idlesson;
	}

	public int getAssessment() {
		return this.assessment;
	}

	public void setAssessment(int assessment) {
		this.assessment = assessment;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getTeacher() {
		return teacher;
	}

	public void setTeacher(int teacher) {
		this.teacher = teacher;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assessment;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idlesson;
		result = prime * result + teacher;
		result = prime * result + user;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lesson other = (Lesson) obj;
		if (assessment != other.assessment)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idlesson != other.idlesson)
			return false;
		if (teacher != other.teacher)
			return false;
		if (user != other.user)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lesson [idlesson=" + idlesson + ", assessment=" + assessment + ", date=" + date + ", teacher=" + teacher
				+ ", user=" + user + "]";
	}	
}