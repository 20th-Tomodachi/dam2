package com.rubencarmona.myteacher.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rubencarmona.myteacher.domain.AuthoritiesUsers;

@Repository
public interface AuthoritiesUsersRepository extends JpaRepository<AuthoritiesUsers, Integer> {

}
