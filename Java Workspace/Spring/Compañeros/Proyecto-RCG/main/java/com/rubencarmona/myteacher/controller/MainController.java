package com.rubencarmona.myteacher.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
		
	@GetMapping({"/","/index","/index.html"})
	public String index() {
		return "index";
	}
	
	@GetMapping({"/contact.html","/contact"})
	public String contact() {		
		return "contact";
	};
	
	@GetMapping({"/about.html","/about"})
	public String about() {		
		return "about";
	};
	
	@GetMapping({"/level.html","/level"})
	public String level() {		
		return "level";
	};
	
	@GetMapping({"/method.html","/method"})
	public String method() {		
		return "method";
	};
	
	@GetMapping({"/prices.html","/prices"})
	public String prices() {		
		return "prices";
	};
	
	@GetMapping({"/teachers.html","/teachers"})
	public String teacher() {		
		return "teachers";
	};

	@GetMapping({"/private/user", "/private/user.html"})
	public String privateUser() {
	return "/private/user";
	}
	
	@GetMapping({"/private/profile", "/private/profile.html"})
	public String privateProfile() {
	return "/private/profile";
	}
	
	@GetMapping({"/private/calendar", "/private/calendar.html"})
	public String privateCalendar() {
	return "/private/calendar";
	}
	
	
}
