package com.rubencarmona.myteacher.domain;



import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the authority database table.
 * 
 */
@Entity
@NamedQuery(name="Authority.findAll", query="SELECT a FROM Authority a")
public class Authority implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int authorityid;

	private String authority;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="authorities")
	private List<User> authorities;

	public Authority() {
	}

	public int getAuthorityid() {
		return this.authorityid;
	}

	public void setAuthorityid(int authorityid) {
		this.authorityid = authorityid;
	}

	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public List<User> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(List<User> authorities) {
		this.authorities = authorities;
	}

}