package com.rubencarmona.myteacher.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.rubencarmona.myteacher.domain.User;
import com.rubencarmona.myteacher.service.MyTeacherDAOService;

@Controller
public class LoginController {
	


	
	@GetMapping({"/login.html","/login"})
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}

}
