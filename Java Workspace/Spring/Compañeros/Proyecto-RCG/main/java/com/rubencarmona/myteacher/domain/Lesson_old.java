package com.rubencarmona.myteacher.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import java.util.Date;


/**
 * The persistent class for the lesson database table.
 * 
 */
//@Entity
//@EntityListeners(AuditingEntityListener.class)
//@NamedQuery(name="Lesson.findAll", query="SELECT l FROM Lesson l")
public class Lesson_old implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idlesson;

	@Column
	private int assessment;

	@Column
	private Date date;

	//bi-directional many-to-one association to Teacher
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="teacherid")
	private Teacher teacher;

	//bi-directional many-to-one association to User
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="userid")
	private User user;

	public Lesson_old() {
	}
	
	public Lesson_old(Teacher teacher, User user, Date date) {
		super();
		this.assessment = 0;
		this.date = date;
		this.teacher = teacher;
		this.user = user;
	}
	
	

	public int getIdlesson() {
		return this.idlesson;
	}

	public void setIdlesson(int idlesson) {
		this.idlesson = idlesson;
	}

	public int getAssessment() {
		return this.assessment;
	}

	public void setAssessment(int assessment) {
		this.assessment = assessment;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Teacher getTeacher() {
		return this.teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assessment;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idlesson;
		result = prime * result + ((teacher == null) ? 0 : teacher.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lesson_old other = (Lesson_old) obj;
		if (assessment != other.assessment)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idlesson != other.idlesson)
			return false;
		if (teacher == null) {
			if (other.teacher != null)
				return false;
		} else if (!teacher.equals(other.teacher))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lesson [idlesson=" + idlesson + ", assessment=" + assessment + ", date=" + date + ", teacher=" + teacher
				+ ", user=" + user + "]";
	}	
}