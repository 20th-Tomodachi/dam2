package com.rubencarmona.myteacher.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.rubencarmona.myteacher.domain.User;
import com.rubencarmona.myteacher.service.MyTeacherDAOService;

@Controller
public class RegisterController {
	
	@Autowired
	MyTeacherDAOService  myTecherService;
	

	@PostMapping({"/register.html", "/register"})
	public String register(@ModelAttribute User user) {
		myTecherService.saveUser(user);
		return "redirect:/login";
		}
	}

