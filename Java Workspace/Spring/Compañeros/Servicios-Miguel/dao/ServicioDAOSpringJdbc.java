

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pfc.clientes.dominio.Cliente;

@Repository
@Transactional
public class ServicioDAOSpringJdbc implements ServicioDAOInterface {

	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate;}

	public ServicioDAOSpringJdbc() {
	}

	public boolean alta(Servicio s) {
		try 
		{
			String SQL = "INSERT INTO TServicio VALUES (?,?,?,?,?)";
			jdbcTemplate.update(SQL, c.getIdServicio(), c.getDescripcion(),c.getFecha(),c.getImporte(),c.getIdCliente());
			
		} catch ( DataAccessException ex ) {return false;}

		return true;
	}

	public void baja(String idServicio) {
		String SQL = "DELETE FROM TServicio where IdServicio = ?";
		jdbcTemplate.update(SQL, idServicio);
		
	};

	public void modificacion(Servicio s) {
		String SQL = "UPDATE TServicio SET Descripcion = ?, Fecha= ?, Importe= ?, IdCliente= ? WHERE IdServicio = ?";
		jdbcTemplate.update(SQL, s.getDescripcion(), s.getFecha(),s.getImporte(),s.getIdCliente(),s.getIdServicio());
		
	}
	
	@Transactional(readOnly=true)
	public Servicio consulta(String idServicio) {
		String SQL = "SELECT * FROM TServicio WHERE IdServicio = ?";
		Servicio servicio = jdbcTemplate.queryForObject(SQL,new Object[] { idServicio }, new ServicioMapper());
		return servicio;
	}

	
	@Transactional(readOnly=true)
	public List<Servicio> consultaAll() {
		String SQL = "SELECT * FROM TServicio";
		List<Servicio> todoslosServicios = jdbcTemplate.query(SQL, new ServicioMapper());
		return todoslosServicios;
	}
}

class ServicioMapper implements RowMapper<Servicio> {

	public Servicio mapRow(ResultSet rs, int rowNum) throws SQLException 
	{
		Servicio servicio = new Servicio();
		servicio.setIdCliente(rs.getString("IdCliente"));
		servicio.setEmail(rs.getString("Email"));
		servicio.setProvincia(rs.getString("Provincia"));
		servicio.setNombre(rs.getString("Nombre"));
		servicio.setIdServicio(rs.getString("IdServicio"));
		return servicio;
	}
}
