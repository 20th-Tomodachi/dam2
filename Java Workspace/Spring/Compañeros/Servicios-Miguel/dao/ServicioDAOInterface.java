

import java.util.List;

import pfc.clientes.dominio.Cliente;

public interface ServicioDAOInterface {
	
	public boolean alta(Servicio s);
	public void baja(String idServicio);
	public void modificacion(Servicio s);
	public Servicio consulta(String idServicio );
	public List<Servicio> consultaAll();
}
