

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pfc.clientes.dominio.Cliente;

@Repository
@Transactional
public class ClienteDAOSpringJdbc implements ClienteDAOInterface {

	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate;}

	public ClienteDAOSpringJdbc() {
	}

	public boolean alta(Cliente c) {
		try 
		{
			String SQL = "INSERT INTO TClientes VALUES (?,?,?,?,?)";
			jdbcTemplate.update(SQL, c.getIdCliente(), c.getEmail(),c.getProvincia(),c.getNombre(),c.getIdServicio());
			
		} catch ( DataAccessException ex ) {return false;}

		return true;
	}

	public void baja(String idCliente) {
		String SQL = "DELETE FROM TClientes where IdCliente = ?";
		jdbcTemplate.update(SQL, idCliente);
		
	};

	public void modificacion(Cliente c) {
		String SQL = "UPDATE TClientes SET Email = ?, Provincia= ?, Nombre= ?, IdServicio= ? WHERE IdCliente = ?";
		jdbcTemplate.update(SQL, c.getEmail(), s.getProvincia(), c.getNombre(),c.getIdServicio(),s.getIdCliente());
		
	}
	
	@Transactional(readOnly=true)
	public Cliente consulta(String idCliente) {
		String SQL = "SELECT * FROM TClientes WHERE IdCliente = ?";
		Cliente cliente = jdbcTemplate.queryForObject(SQL,new Object[] { idCliente }, new ClienteMapper());
		return cliente;
	}

	
	@Transactional(readOnly=true)
	public List<Cliente> consultaAll() {
		String SQL = "SELECT * FROM TClientes";
		List<Cliente> todoslosClientes = jdbcTemplate.query(SQL, new ClienteMapper());
		return todoslosClientes;
	}
}

class ClienteMapper implements RowMapper<Cliente> {

	public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException 
	{
		Cliente cliente = new Cliente();
		cliente.setIdCliente(rs.getString("IdCliente"));
		cliente.setEmail(rs.getString("Email"));
		cliente.setProvincia(rs.getString("Provincia"));
		cliente.setNombre(rs.getString("Nombre"));
		cliente.setIdServicio(rs.getString("IdServicio"));
		return cliente;
	}
}
