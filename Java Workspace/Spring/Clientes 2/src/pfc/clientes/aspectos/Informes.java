package pfc.clientes.aspectos;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

import pfc.clientes.dominio.Cliente;


@Aspect
public class Informes {
	
	private String emailProyectoId; 
	private String emailProyectoPw; 
	private String emailAdmin;  
	
    @After("execution(* pfc.clientes.dao.ClienteDAOSpringJdbc.baja(..))")
	public void informeBaja() {
		String mensajeAsunto = "Informe del proyecto Clientes";
		String mensajeTexto  = "Un cliente se ha dado de baja";
		enviarCorreo(emailProyectoId,emailProyectoPw,emailAdmin, mensajeAsunto,mensajeTexto);
		
		// C�digo de comprobaci�n 
		System.out.println("Ejuecutado el advice InformeBaja()");
	}
    
    @After("execution(* pfc.clientes.dao.ClienteDAOSpringJdbc.alta(..)) && args(cliente)")
	public void informeAlta(Cliente cliente) {
    	
		String mensajeAsunto = "Informe del proyecto Clientes";
		String mensajeTexto  = "Un nuevo cliente: " + cliente.getNombre() + " se ha dado de alta";
		enviarCorreo(emailProyectoId,emailProyectoPw,emailAdmin, mensajeAsunto,mensajeTexto);
		
		// C�digo de comprobaci�n 
		System.out.println("Ejuecutado el advice InformeAlta(). Cliente " + cliente.getNombre());
		
	}

	
	public void enviarCorreo(String emailOrigenId, String emailOrigenPw, String emailDestino, String mensajeAsunto, String mensajeTexto) {

		/*
		 * C�digo desarrollado �ntegramente en el proyecto Despensalia 
		 * 
		 */
	}
	
	public void setEmailProyectoId(String emailProyectoId) {this.emailProyectoId = emailProyectoId;}
	public void setEmailProyectoPw(String emailProyectoPw) {this.emailProyectoPw = emailProyectoPw;}
	public void setEmailAdmin(String emailAdmin) {this.emailAdmin = emailAdmin;}
	
}
